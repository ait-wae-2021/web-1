Rails.application.routes.draw do
  get 'home/indexx'
  resources :friends
  devise_for :users
  root 'home#indexx'
  
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
